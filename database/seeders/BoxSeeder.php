<?php

namespace Database\Seeders;

use App\Models\Box;
use Illuminate\Database\Seeder;

class BoxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Box::insert(
            array(
                // First Row
                    [
                        'id'                => 1,
                        'box_id'            => 1,
                        'title'             => '',
                        'row_position'      => 1,
                        'column_position'   => 1,
                        'color_style'       => 'aqua',
                        'text_style'        => 'Arial'
                    ],
                    [
                        'id'                => 2,
                        'box_id'            => 2,
                        'title'             => 'Dropee.com',
                        'row_position'      => 1,
                        'column_position'   => 2,
                        'color_style'       => 'aquamarine',
                        'text_style'        => 'Times New Roman'
                    ],
                    [
                        'id'                => 3,
                        'box_id'            => 3,
                        'title'             => '',
                        'row_position'      => 1,
                        'column_position'   => 3,
                        'color_style'       => 'darkorange',
                        'text_style'        => 'Lucida Console'
                    ],
                    [
                        'id'                => 4,
                        'box_id'            => 4,
                        'title'             => 'Build Trust',
                        'row_position'      => 1,
                        'column_position'   => 4,
                        'color_style'       => 'darkgreen',
                        'text_style'        => 'Verdana'
                    ],
                // First Row

                // Second Row
                    [
                        'id'                => 5,
                        'box_id'            => 5,
                        'title'             => '',
                        'row_position'      => 2,
                        'column_position'   => 1,
                        'color_style'       => 'aqua',
                        'text_style'        => 'Arial'
                    ],
                    [
                        'id'                => 6,
                        'box_id'            => 6,
                        'title'             => '',
                        'row_position'      => 2,
                        'column_position'   => 2,
                        'color_style'       => 'aquamarine',
                        'text_style'        => 'Times New Roman'
                    ],
                    [
                        'id'                => 7,
                        'box_id'            => 7,
                        'title'             => 'Saas enabled marketplace',
                        'row_position'      => 2,
                        'column_position'   => 3,
                        'color_style'       => 'darkorange',
                        'text_style'        => 'Lucida Console'
                    ],
                    [
                        'id'                => 8,
                        'box_id'            => 8,
                        'title'             => '',
                        'row_position'      => 2,
                        'column_position'   => 4,
                        'color_style'       => 'darkgreen',
                        'text_style'        => 'Verdana'
                    ],
                // Second Row

                // Third Row
                    [
                        'id'                => 9,
                        'box_id'            => 9,
                        'title'             => 'B2B marketplace',
                        'row_position'      => 3,
                        'column_position'   => 1,
                        'color_style'       => 'aqua',
                        'text_style'        => 'Arial'
                    ],
                    [
                        'id'                => 10,
                        'box_id'            => 10,
                        'title'             => '',
                        'row_position'      => 3,
                        'column_position'   => 2,
                        'color_style'       => 'aquamarine',
                        'text_style'        => 'Times New Roman'
                    ],
                    [
                        'id'                => 11,
                        'box_id'            => 11,
                        'title'             => '',
                        'row_position'      => 3,
                        'column_position'   => 3,
                        'color_style'       => 'darkorange',
                        'text_style'        => 'Lucida Console'
                    ],
                    [
                        'id'                => 12,
                        'box_id'            => 12,
                        'title'             => '',
                        'row_position'      => 3,
                        'column_position'   => 4,
                        'color_style'       => 'darkgreen',
                        'text_style'        => 'Verdana'
                    ],
                // Third Row

                // Fourth Row
                    [
                        'id'                => 13,
                        'box_id'            => 13,
                        'title'             => '',
                        'row_position'      => 4,
                        'column_position'   => 1,
                        'color_style'       => 'aqua',
                        'text_style'        => 'Arial'
                    ],
                    [
                        'id'                => 14,
                        'box_id'            => 14,
                        'title'             => '',
                        'row_position'      => 4,
                        'column_position'   => 2,
                        'color_style'       => 'aquamarine',
                        'text_style'        => 'Times New Roman'
                    ],
                    [
                        'id'                => 15,
                        'box_id'            => 15,
                        'title'             => '',
                        'row_position'      => 4,
                        'column_position'   => 3,
                        'color_style'       => 'darkorange',
                        'text_style'        => 'Lucida Console'
                    ],
                    [
                        'id'                => 16,
                        'box_id'            => 16,
                        'title'             => 'Provide tranparency',
                        'row_position'      => 4,
                        'column_position'   => 4,
                        'color_style'       => 'darkgreen',
                        'text_style'        => 'Verdana'
                    ],
                // Fourth Row
            )
        );
    }
}

// 'box_id',
//         'row_position',
//         'column_position',
//         'color_style',
//         'text_style'