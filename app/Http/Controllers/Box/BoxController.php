<?php

namespace App\Http\Controllers\Box;

use App\Http\Controllers\Controller;
use App\Models\Box;
use Illuminate\Http\Request;

class BoxController extends Controller
{
    public function box2(Request $request){
        return redirect('/box');
    }

    public function box(Request $request){

        $boxes = Box::all(); 

        $boxes_111 = Box::where('box_id',1)
            ->where('row_position',1)
            ->where('column_position',1)
            ->first();

        $boxes_212 = Box::where('box_id',2)
            ->where('row_position',1)
            ->where('column_position',2)
            ->first();



        $boxes_313 = Box::where('box_id',3)
            ->where('row_position',1)
            ->where('column_position',3)
            ->first();

        $boxes_414 = Box::where('box_id',4)
            ->where('row_position',1)
            ->where('column_position',4)
            ->first();

        $boxes_521 = Box::where('box_id',5)
            ->where('row_position',2)
            ->where('column_position',1)
            ->first();

        $boxes_622 = Box::where('box_id',6)
            ->where('row_position',2)
            ->where('column_position',2)
            ->first();

        $boxes_723 = Box::where('box_id',7)
            ->where('row_position',2)
            ->where('column_position',3)
            ->first();

        $boxes_824 = Box::where('box_id',8)
            ->where('row_position',2)
            ->where('column_position',4)
            ->first();

        $boxes_931 = Box::where('box_id',9)
            ->where('row_position',3)
            ->where('column_position',1)
            ->first();

        $boxes_1032 = Box::where('box_id',10)
            ->where('row_position',3)
            ->where('column_position',2)
            ->first();

        $boxes_1133 = Box::where('box_id',11)
            ->where('row_position',3)
            ->where('column_position',3)
            ->first();

        $boxes_1234 = Box::where('box_id',12)
            ->where('row_position',3)
            ->where('column_position',4)
            ->first();

        $boxes_1341 = Box::where('box_id',13)
            ->where('row_position',4)
            ->where('column_position',1)
            ->first();

        $boxes_1442 = Box::where('box_id',14)
            ->where('row_position',4)
            ->where('column_position',2)
            ->first();

        $boxes_1543 = Box::where('box_id',15)
            ->where('row_position',4)
            ->where('column_position',3)
            ->first();

        $boxes_1644 = Box::where('box_id',16)
            ->where('row_position',4)
            ->where('column_position',4)
            ->first();


        return view('box', compact(
                                    'boxes_111',
                                    'boxes_212',
                                    'boxes_313',
                                    'boxes_414',
                                    'boxes_521',
                                    'boxes_622',
                                    'boxes_723',
                                    'boxes_824',
                                    'boxes_931',
                                    'boxes_1032',
                                    'boxes_1133',
                                    'boxes_1234',
                                    'boxes_1341',
                                    'boxes_1442',
                                    'boxes_1543',
                                    'boxes_1644'
                                ));

    }

    public function getBoxValue(Request $request){

        $id                 = $request->input('box_id');
        $row_position       = $request->input('row_position');
        $column_position    = $request->input('column_position');

        $boxes = Box::where('box_id',$id)
            ->where('row_position',$row_position)
            ->where('column_position',$column_position)
            ->get();

        return json_encode($boxes, true);

    }

    public function changeBox(Request $request){

        // Request
            $getid                  = $request->input('getid');
            $box_id                 = $request->input('box_id');

            $title                  = $request->input('title_in');
            // $cur_title              = $request->input('cur_title_in');

            $new_row_position       = $request->input('row_position');
            $cur_row_position       = $request->input('cur_row_position');

            $new_column_position    = $request->input('column_position');
            $cur_column_position    = $request->input('cur_column_position');

            $new_color_style        = $request->input('color_style');
            $cur_color_style        = $request->input('cur_color_style');

            $new_text_style         = $request->input('text_style');
            $cur_text_style         = $request->input('cur_text_style');
        // Request

        // Check
            $checks = Box::where('row_position', $request->input('row_position'))
                ->where('column_position',$request->input('column_position'))
                ->get(); 
            $j          = 0;  
            $check_id   = ''; 
            foreach ($checks as $check) { 
                if($j==0){  
                    $check_id         = $check->id;   
                }
                else{  
                    $check_id         = $check_id.','.$check->id;   
                }
                    $j++;
            } 
        // Check

        if($getid == $check_id ){

            // Update New
                Box::where('id',$getid)->update([
                    'color_style'       => $new_color_style,
                    'text_style'        => $new_text_style
                ]); 
            // Update New

            return back()->with('status', 'Box updated')
                            ->with('message', 'Success')
                            ->with('code', 'success');


        } else {

            
            // Validation
                $request->validate(
                    [
                        'row_position'              => 'required',
                        'column_position'           => 'required',
                        'color_style'               => 'required',
                        'text_style'                => 'required', 
                    ],[
                        'row_position.required'     => 'Position of row is required',
                        'column_position.required'  => 'Position of column is required',
                        'color_style.required'      => 'Color is required',
                        'text_style.required'       => 'Font Style is required',  
                    ]
                );
            // Validation

            // Get Point To Info
                $news = Box::where('row_position', $new_row_position)
                    ->where('column_position',$new_column_position)
                    ->get(); 
                $j          = 0;  
                $new_id     = '';
                $new_box_id = '';
                $new_title  = '';
                $new_row    = '';
                $new_col    = '';
                $new_color  = '';
                $new_text   = ''; 
                foreach ($news as $new) { 
                    if($j==0){  
                        $new_id         = $new->id;  
                        $new_box_id     = $new->box_id;  
                        $new_title      = $new->title;  
                        $new_row        = $new->row_position;  
                        $new_col        = $new->column_position;  
                        $new_color      = $new->color_style;  
                        $new_text       = $new->text_style;  
                    }
                    else{  
                        $new_id         = $new_id.','.$new->id;  
                        $new_box_id     = $new_box_id.','.$new->box_id;  
                        $new_title      = $new_title.','.$new->title;  
                        $new_row        = $new_row.','.$new->row_position;  
                        $new_col        = $new_col.','.$new->column_position;  
                        $new_color      = $new_color.','.$new->color_style;  
                        $new_text       = $new_text.','.$new->text_style;  
                    }
                        $j++;
                } 
            // Get Point To Info

            // Get Current Info
                $currents = Box::where('row_position', $cur_row_position)
                    ->where('column_position',$cur_column_position)
                    ->get(); 
                $j                      = 0;  
                $current_id             = '';
                $current_box_id         = '';
                $current_title          = '';
                $current_row            = '';
                $current_col            = '';
                $current_color_style    = '';
                $current_text_style     = ''; 
                foreach ($currents as $current) { 
                    if($j==0){  
                        $current_id                 = $current->id;  
                        $current_box_id             = $current->box_id;  
                        $current_title              = $current->title;  
                        $current_row                = $current->row_position;  
                        $current_col                = $current->column_position;  
                        $current_color_style        = $current->color_style;  
                        $current_text_style         = $current->text_style;  
                    }
                    else{  
                        $current_id                 = $current_id.','.$current->id;  
                        $current_box_id             = $current_box_id.','.$current->box_id;  
                        $current_title              = $current_title.','.$current->title;  
                        $current_row                = $current_row.','.$current->row_position;  
                        $current_col                = $current_col.','.$current->column_position;  
                        $current_color_style        = $current_color_style.','.$current->color_style;  
                        $current_text_style         = $current_text_style.','.$current->text_style;  
                    }
                        $j++;
                } 
            // Get Current Info

            // Update Current
                Box::where('id',$current_id)->update([ 
                    'box_id'            => $new_box_id,
                    'title'             => $title,
                    'row_position'      => $new_row_position,
                    'column_position'   => $new_column_position,
                    'color_style'       => $new_color_style,
                    'text_style'        => $new_text_style
                ]); 
            // Update Current

            // Update Point To
                Box::where('id',$new_id)->update([ 
                    'box_id'            => $box_id,
                    'title'             => $new_title,
                    'row_position'      => $cur_row_position,
                    'column_position'   => $cur_column_position,
                    'color_style'       => $cur_color_style,
                    'text_style'        => $cur_text_style
                ]); 
            // Update Point To    

            return back()->with('status', 'Box updated')
                            ->with('message', 'Success')
                            ->with('code', 'success');

        }  

    }
}
