<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    use HasFactory;

    protected $table        = 'boxes';
    protected $connection   = 'pgsql';
    public $incrementing    = true;
    public $timestamps      = false;
    protected $guarded      = [ 
        'box_id',
        'row_position',
        'column_position',
        'color_style',
        'text_style'
    ]; 

}
