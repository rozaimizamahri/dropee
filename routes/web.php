<?php

use App\Http\Controllers\Box\BoxController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Box
    Route::get('/',             [BoxController::class, 'box2']);
    Route::get('/box',          [BoxController::class, 'box']);
    Route::get('/getBoxValue',  [BoxController::class, 'getBoxValue']);
    Route::post('/changeBox',   [BoxController::class, 'changeBox']);
// Box






 


 