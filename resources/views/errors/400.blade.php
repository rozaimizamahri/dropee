<!DOCTYPE html>
@extends('layouts.error')

@section('title')
    404
@endsection

@section('css')
@endsection

@section('content')

    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
        
        <div class="error-body text-center">
            <h1>400</h1>
            <h3 class="text-uppercase">Page Not Found !</h3>
            <p class="text-muted m-t-30 m-b-30">Page you are trying to search was not found.</p>
            <p>{{$errors}}</p>
            <a href="{{url('/')}}" class="btn btn-warning btn-rounded waves-effect waves-light m-b-40">Back to home</a> 
        </div>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

@endsection

@section('js')
@endsection