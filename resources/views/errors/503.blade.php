<!DOCTYPE html>
@extends('layouts.error')

@section('title')
    503
@endsection

@section('css')
@endsection

@section('content')

    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== --> 
    
        <div class="error-body text-center">
            <h1>503</h1>
            <h3 class="text-uppercase">This site is getting up in few minutes. </h3>
            <p class="text-muted m-t-30 m-b-30">Please try after some time</p>
            <p>{{$errors}}</p>
            <a href="{{url('/home')}}" class="btn btn-warning btn-rounded waves-effect waves-light m-b-40">Back to home</a> 
        </div>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

@endsection

@section('js')
@endsection