<!DOCTYPE html>
@extends('layouts.error')

@section('title')
    500
@endsection

@section('css')
@endsection

@section('content')

    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== --> 

        <div class="error-body text-center">
            <h1>500</h1>
            <h3 class="text-uppercase">Internal Server Error !</h3>
            <p class="text-muted m-t-30 m-b-30">Error</p> 
            @if($errors)
            {{$errors}}<br/>
            @endif

            @if($codes)
            {{$codes}}<br/>
            @endif

            @if($messages)
            {{$messages}}<br/><br/>
            @endif
            <a href="{{url('/')}}" class="btn btn-warning btn-rounded waves-effect waves-light m-b-40">Back to home</a> 
        </div>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

@endsection

@section('js')
@endsection