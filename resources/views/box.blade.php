<!DOCTYPE html>
@extends('layouts.box')

@section('title')
    Dropee
@endsection

@section('css')
@endsection

@section('content')

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">  

                    <div class="card card-outline-info">
                        <div class="card-header"> 
                            <h4 class="m-b-0 text-white">Dropee Box</h4>
                        </div>
                        <div class="card-body"> 

                            <div class="row"> 

                                <!-- Modal : Changes -->
                                    <div id="modal_change" class="modal fade bs-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none; overflow-y:auto;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myLargeModalLabel">Fill in details (Fields <font color="red">*</font> asterisk required)</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body">

                                                    <form id='form-change' class="form-change" name="form-change" action="{{url('/changeBox')}}" method="post"> 
                                                    @csrf  

                                                        <input type="hidden" id="getid" class="form-control getid" name='getid' value="" placeholder="" readonly="readonly">
                                                        <input type="hidden" id="box_id" class="form-control box_id" name="box_id" value="" placeholder="" readonly="readonly"/> 
                                                         

                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3">Text</label>
                                                                <div class="col-md-9">  
                                                                    <span id="title"></span>
                                                                    <input type="hidden" id="title_in" class="form-control title_in" name="title_in" value="" placeholder="" readonly="readonly"/>
                                                                    <input type="hidden" id="cur_title_in" class="form-control cur_title_in" name="cur_title_in" value="" placeholder="" readonly="readonly"/>
                                                                </div>
                                                            </div>
                                                        </div>  

                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3"><font color="red">*</font> Row Position</label>
                                                                <div class="col-md-9">  
                                                                    <input type="hidden" id="cur_row_position" class="form-control cur_row_position" name="cur_row_position" value="" placeholder="" readonly="readonly"/>
                                                                    <select id="row_position" class="form-control select2 custom-select row_position" name="row_position" style="width: 100%;"> 
                                                                        <option value="" selected="selected">Select Row</option>  
                                                                        <option value="1">1</option>  
                                                                        <option value="2">2</option>  
                                                                        <option value="3">3</option>  
                                                                        <option value="4">4</option>  
                                                                    </select>
                                                                    <small class="form-control-feedback"></small> 
                                                                </div>
                                                            </div>
                                                        </div>   

                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3"><font color="red">*</font> Column Position</label>
                                                                <div class="col-md-9">  
                                                                    <input type="hidden" id="cur_column_position" class="form-control cur_column_position" name="cur_column_position" value="" placeholder="" readonly="readonly"/>
                                                                    <select id="column_position" class="form-control select2 custom-select column_position" name="column_position" style="width: 100%;"> 
                                                                        <option value="" selected="selected">Select Column</option>  
                                                                        <option value="1">1</option>  
                                                                        <option value="2">2</option>  
                                                                        <option value="3">3</option>  
                                                                        <option value="4">4</option>  
                                                                    </select>
                                                                    <small class="form-control-feedback"></small> 
                                                                </div>
                                                            </div>
                                                        </div> 

                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3"><font color="red">*</font> Color </label>
                                                                <div class="col-md-9">  
                                                                    <input type="hidden" id="cur_color_style" class="form-control cur_color_style" name="cur_color_style" value="" placeholder="" readonly="readonly"/>
                                                                    <select id="color_style" class="form-control select2 custom-select color_style" name="color_style" style="width: 100%;"> 
                                                                        <option value="" selected="selected">Select Color</option>  
                                                                        <option value="aqua">Aqua</option>  
                                                                        <option value="aquamarine">Aquamarine</option>  
                                                                        <option value="blue">Blue</option>  
                                                                        <option value="blueviolet">Blueviolet</option>  
                                                                        <option value="brown">Brown</option>  
                                                                        <option value="chocolate">Chocolate</option>  
                                                                        <option value="coral">Coral</option>  
                                                                        <option value="cyan">Cyan</option>  
                                                                        <option value="crimson">Crimson</option>  
                                                                        <option value="darkgreen">Dark Green</option>  
                                                                        <option value="darkorange">Dark Orange</option>   
                                                                    </select>
                                                                    <small class="form-control-feedback"></small> 
                                                                </div>
                                                            </div>
                                                        </div>  

                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-3"><font style="font-family: 'Times New Roman', Times, serif;">Font Style</font></label>
                                                                <div class="col-md-9">  
                                                                    <input type="hidden" id="cur_text_style" class="form-control cur_text_style" name="cur_text_style" value="" placeholder="" readonly="readonly"/>
                                                                    <select id="text_style" class="form-control select2 custom-select text_style" name="text_style" style="width: 100%;"> 
                                                                        <option value="" selected="selected">Select Font Style</option>  
                                                                        <option value="Arial">Arial</option>   
                                                                        <option value="Times New Roman">Times New Roman</option>   
                                                                        <option value="Lucida Console">Lucida Console</option>   
                                                                        <option value="Verdana">Verdana</option>   
                                                                    </select>
                                                                    <small class="form-control-feedback"></small> 
                                                                </div>
                                                            </div>
                                                        </div>   

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default waves-effect text-left" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-warning waves-effect text-left" id="info_change" >Update</button>
                                                        </div>

                                                    </form>

                                                </div>
                                            </div>  
                                        </div> 
                                    </div> 
                                <!-- Modal : Changes --> 

                                <!-- List -->
                                    <!-- First Row -->
                                        <!-- Column 1 -->
                                            <div class="col-lg-3">  
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_111->box_id}}','{{$boxes_111->row_position}}','{{$boxes_111->column_position}}')">
                                                    <div class="card">  
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_111->text_style; ?>; color:<?php echo $boxes_111->color_style; ?>; ">{{$boxes_111->title}}</font></h4>  
                                                        </div> 
                                                    </div>
                                                </a> 
                                            </div>
                                        <!-- Column 1 -->

                                        <!-- Column 2 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_212->box_id}}','{{$boxes_212->row_position}}','{{$boxes_212->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_212->text_style; ?>; color:<?php echo $boxes_212->color_style; ?>;">{{$boxes_212->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        <!-- Column 2 -->

                                        <!-- Column 3 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_313->box_id}}','{{$boxes_313->row_position}}','{{$boxes_313->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_313->text_style; ?>; color:<?php echo $boxes_313->color_style; ?>;">{{$boxes_313->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div> 
                                        <!-- Column 3 -->

                                        <!-- Column 4 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_414->box_id}}','{{$boxes_414->row_position}}','{{$boxes_414->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_414->text_style; ?>; color:<?php echo $boxes_414->color_style; ?>;">{{$boxes_414->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>  
                                        <!-- Column 4 -->
                                    <!-- First Row -->

                                    <!-- Second Row -->
                                        <!-- Column 1 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_521->box_id}}','{{$boxes_521->row_position}}','{{$boxes_521->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_521->text_style; ?>; color:<?php echo $boxes_521->color_style; ?>;">{{$boxes_521->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>   
                                        <!-- Column 1 -->

                                        <!-- Column 2 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_622->box_id}}','{{$boxes_622->row_position}}','{{$boxes_622->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_622->text_style; ?>; color:<?php echo $boxes_622->color_style; ?>;">{{$boxes_622->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>   
                                        <!-- Column 2 -->

                                        <!-- Column 3 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_723->box_id}}','{{$boxes_723->row_position}}','{{$boxes_723->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_723->text_style; ?>; color:<?php echo $boxes_723->color_style; ?>;">{{$boxes_723->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>   
                                        <!-- Column 3 -->

                                        <!-- Column 4 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_824->box_id}}','{{$boxes_824->row_position}}','{{$boxes_824->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_824->text_style; ?>; color:<?php echo $boxes_824->color_style; ?>;">{{$boxes_824->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>   
                                        <!-- Column 4 -->
                                    <!-- Second Row -->

                                    <!-- Third Row -->
                                        <!-- Column 1 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_931->box_id}}','{{$boxes_931->row_position}}','{{$boxes_931->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_931->text_style; ?>; color:<?php echo $boxes_931->color_style; ?>;">{{$boxes_931->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>    
                                        <!-- Column 1 -->

                                        <!-- Column 2 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_1032->box_id}}','{{$boxes_1032->row_position}}','{{$boxes_1032->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_1032->text_style; ?>; color:<?php echo $boxes_1032->color_style; ?>;">{{$boxes_1032->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>   
                                        <!-- Column 2 -->

                                        <!-- Column 3 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_1133->box_id}}','{{$boxes_1133->row_position}}','{{$boxes_1133->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_1133->text_style; ?>; color:<?php echo $boxes_1133->color_style; ?>;">{{$boxes_1133->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>    
                                        <!-- Column 3 -->

                                        <!-- Column 4 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_1234->box_id}}','{{$boxes_1234->row_position}}','{{$boxes_1234->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_1234->text_style; ?>; color:<?php echo $boxes_1234->color_style; ?>;">{{$boxes_1234->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>   
                                        <!-- Column 4 -->
                                    <!-- Third Row -->

                                    <!-- Fourth Row -->
                                        <!-- Column 1 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_1341->box_id}}','{{$boxes_1341->row_position}}','{{$boxes_1341->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_1341->text_style; ?>; color:<?php echo $boxes_1341->color_style; ?>;">{{$boxes_1341->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>   
                                        <!-- Column 1 -->

                                        <!-- Column 2 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_1442->box_id}}','{{$boxes_1442->row_position}}','{{$boxes_1442->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_1442->text_style; ?>; color:<?php echo $boxes_1442->color_style; ?>;">{{$boxes_1442->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>    
                                        <!-- Column 2 -->

                                        <!-- Column 3 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_1543->box_id}}','{{$boxes_1543->row_position}}','{{$boxes_1543->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_1543->text_style; ?>; color:<?php echo $boxes_1543->color_style; ?>;">{{$boxes_1543->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>   
                                        <!-- Column 3 -->

                                        <!-- Column 4 -->
                                            <div class="col-lg-3">
                                                <a href="javascript:void(0)" onClick="editBox('{{$boxes_1644->box_id}}','{{$boxes_1644->row_position}}','{{$boxes_1644->column_position}}')">
                                                    <div class="card"> 
                                                        <div class="card-body"> 
                                                            <h4 class="font-normal" style="font-family: <?php echo $boxes_1644->text_style; ?>; color:<?php echo $boxes_1644->color_style; ?>;">{{$boxes_1644->title}}</font></h4>   
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>   
                                        <!-- Column 4 -->
                                    <!-- Fourth Row -->
                                <!-- List -->

                            </div>
                            
                        </div>
                    </div>

                </div>
            </div> 
        </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
   

			
@endsection

@section('js')
 
<!-- Validate -->
<script src="{{ asset('assets-login/plugins/validate/1.14.0/jquery.validate.min.js') }} "></script>

<!-- Session Sweet Aler -->
    @if(session('status'))
        <script>  
            Swal.fire({
                title: '{{session("status")}}',
                icon: 'info',
                html: '{{session("message")}}',
                showCloseButton: true,
                showCancelButton: false,
                focusConfirm: false,
                confirmButtonText:
                    '<i class="fa fa-thumbs-up"></i> Yay!',
                confirmButtonAriaLabel: 'Thumbs up, ok!', 
            })
        </script>            
    @endif
<!-- Session Sweet Aler -->

<script>
    // Modal
        function editBox(getBoxId, getRow, getCol){

            $.ajax({
                url: '<?php echo url("/getBoxValue"); ?>' ,
                type: "GET",
                dataType: "json",
                data: 
                    {
                        "box_id"            : getBoxId,
                        "row_position"      : getRow,
                        "column_position"   : getCol
                    },
                success: function(data)
                {   
                    var ar                      = data;   
                    var getId                   = "";    
                    var box_id                  = "";    
                    var title                   = "";    
                    var row_position            = "";    
                    var column_position         = "";    
                    var color_style             = "";    
                    var text_style              = "";    

                    for (var i = 0; i < ar.length; i++) 
                    {
                        getId                   = ar[i]['id'];          
                        box_id                  = ar[i]['box_id'];          
                        title                   = ar[i]['title'];          
                        row_position            = ar[i]['row_position'];          
                        column_position         = ar[i]['column_position'];          
                        color_style             = ar[i]['color_style'];          
                        text_style              = ar[i]['text_style'];          
                    }	 

                    $('#getid').val(getId);  
                    $('#box_id').val(box_id);  
                    
                    $('#title').html(title);  
                    $('#cur_title_in').val(title);  
                    $('#title_in').val(title);  

                    $('#row_position').val(row_position).change();  
                    $('#cur_row_position').val(row_position);  

                    $('#column_position').val(column_position).change();  
                    $('#cur_column_position').val(column_position);

                    $('#color_style').val(color_style).change();  
                    $('#cur_color_style').val(color_style);

                    $('#text_style').val(text_style).change();  
                    $('#cur_text_style').val(text_style);  
                    
                    $('#modal_change').modal({
                        show: true
                    });  
                },
                error: function(error){
                    console.log("Error:");
                    console.log(error);
                }
            });    
        } 
    // Modal

    // Checking 
        $(function() {
            $("#form-change").validate({
                rules: {
                    row_position: {
                    required: true
                    },  
                    column_position: {
                    required: true
                    },
                    color_style: {
                    required: true
                    },  
                    text_style: {
                    required: true
                    },    
                },
                messages: {
                    row_position: {
                    required: "<font color='red'>* Cannot be empty</font>"
                    },  
                    column_position: {
                    required: "<font color='red'>* Cannot be empty</font>"
                    },  
                    color_style: {
                    required: "<font color='red'>* Cannot be empty</font>"
                    },
                    text_style: {
                    required: "<font color='red'>* Cannot be empty</font>"
                    }, 
                }
            });
        });
    // Checking

    // Edit Data
        $('#info_change').click(function(){ 
            if (!$('#form-change').valid()) 
            {
                e.preventDefault()
            }
            else 
            {  
                const swalWithBootstrapButtons = Swal.mixin({
                customClass: { 
                    cancelButton: 'btn btn-danger',
                    confirmButton: 'btn btn-success'
                },
                buttonsStyling: false,
                })
                swalWithBootstrapButtons.fire({
                title:'Confirm to change details ?',
                text: "",
                type: 'warning',
                showCancelButton: true, 
                cancelButtonText: 'Cancel',
                confirmButtonText: 'Change',
                reverseButtons: true
                }).then((result) => {
                if (result.value) {
                    $( "#form-change" ).submit();
                } else if ( 
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Record has not been saved.',
                    'error'
                    )
                }
                })  
            }
        });
    // Edit Data
</script>

@endsection
