<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets-login/images/dropee.png') }} ">
    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets-login/plugins/bootstrap/css/bootstrap.min.css') }} " rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('assets-login/css/style.css') }} " rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{ asset('assets-login/css/colors/blue.css') }} " id="theme" rel="stylesheet">

    <!-- Sweet Alert 2 Js -->
    <link rel="stylesheet" href="{{ asset('assets-login/plugins/sweetalert2/css/sweetalert2.css') }} ">
    <link rel="stylesheet" href="{{ asset('assets-login/plugins/sweetalert2/css/sweetalert2.min.css') }} ">

    <!-- Select 2 --> 
    <link href="{{ asset('assets-login/plugins/select2/dist/css/select2.min.css') }} " rel="stylesheet" type="text/css" /> 
    <link href="{{ asset('assets-login/plugins/bootstrap-select/bootstrap-select.min.css') }} " rel="stylesheet" /> 
    <link href="{{ asset('assets-login/plugins/multiselect/css/multi-select.css') }} " rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
      .textarealah{  
        display: block;
        box-sizing: padding-box;
        overflow: hidden;
        resize: vertical;
        padding: 5px 8px;
        font-size: 11px;
        border-radius: 0;
        border: 1px solid #ccc;
        width:100%;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        min-height: 30px;
      }  

      .loader {
            position:fixed;
            z-index: 99;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: white;
            opacity:0.8;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .loader > img {
            width: 100px;
        }

        .loader.hidden {
            animation: fadeOut 1s;
            animation-fill-mode: forwards;
        }

        @keyframes fadeOut {
            100% {
                opacity: 0;
                visibility: hidden;
            }
        }
    </style>

@yield('css')
</head>

<body class="fix-header card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="javascript:void(0)">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="{{ asset('assets-login/images/background/dropee.png') }} " alt="DROPEE" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="{{ asset('assets-login/images/background/dropee.png') }} " alt="DROPEE" class="light-logo" width="100" height="auto" />
                        </b> 
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            
                            
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                             
                            
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown mega-dropdown"> <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-home"></i></a>
                             
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0"> 
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        
         

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            @yield('content')
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © <?php echo date('Y'); ?> Dropee <img src="{{ asset('assets-login/images/background/dropee.png') }} " alt="user-img" class="" height="45px"></footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('assets-login/plugins/jquery/jquery.min.js') }} "></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('assets-login/plugins/bootstrap/js/popper.min.js') }} "></script>
    <script src="{{ asset('assets-login/plugins/bootstrap/js/bootstrap.min.js') }} "></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('assets-login/js/jquery.slimscroll.js') }} "></script>
    <!--Wave Effects -->
    <script src="{{ asset('assets-login/js/waves.js') }} "></script>
    <!--Menu sidebar -->
    <script src="{{ asset('assets-login/js/sidebarmenu.js') }} "></script>
    <!--stickey kit -->
    <script src="{{ asset('assets-login/plugins/sticky-kit-master/dist/sticky-kit.min.js') }} "></script>
    <script src="{{ asset('assets-login/plugins/sparkline/jquery.sparkline.min.js') }} "></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('assets-login/js/custom.min.js') }} "></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{ asset('assets-login/plugins/styleswitcher/jQuery.style.switcher.js') }} "></script>


    <!-- Select 2 -->
    <script src="{{ asset('assets-login/plugins/select2/dist/js/select2.full.min.js') }} " type="text/javascript"></script>
    <script src="{{ asset('assets-login/plugins/bootstrap-select/bootstrap-select.min.js') }} " type="text/javascript"></script> 

    <!-- Sweet Alert 2 Js -->
    <script src="{{ asset('assets-login/plugins/sweetalert2/js/sweetalert2.js') }} "></script>
    <script src="{{ asset('assets-login/plugins/sweetalert2/js/sweetalert2.min.js') }} "></script>


    <!-- Input Number -->
        <script>

            $(document).ready(function(){
                $(".select2").select2();
            });


            $(document).ready(function(){
                // Number Only
                    $(".input_number").on("keypress keyup blur",function (event) {    
                    $(this).val($(this).val().replace(/[^\d].+/, ""));
                        if ((event.which < 48 || event.which > 57)) {
                            event.preventDefault();
                        }
                    }); 
                // Number Only

                // Number Text Only 
                    $(".input_number_text").keypress(function(event){
                        var ew = event.which;
                        if(48 <= ew && ew <= 57)
                            return true;
                        if(65 <= ew && ew <= 90)
                            return true;
                        if(97 <= ew && ew <= 122)
                            return true;
                        return false;
                    }); 
                // Number Text Only

                // THOUSAND SEPARATOR COMAS
                    $('input.input_thousand_separator').keyup(function(event) {
                        // skip for arrow keys
                        if(event.which >= 37 && event.which <= 40) return;

                        // format number
                        $(this).val(function(index, value) {
                        return value
                        // Allow separator with 2 decimals point but this code can allow user to add multiple dot line.
                        // .replace(/[^\d.]/g, "")
                        // .replace(/\.(\d{2})\d+/, '.$1')
                        // .replace(/\B(?=(\d{3})+(?!\d))/g, ",")

                        // Keep only digits and decimal points:
                        .replace(/[^\d.]/g, "")
                        // Remove duplicated decimal point, if one exists:
                        .replace(/^(\d*\.)(.*)\.(.*)$/, '$1$2$3')
                        // Keep only two digits past the decimal point:
                        .replace(/\.(\d{2})\d+/, '.$1')
                        // Add thousands separators:
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")

                        ;
                        });
                    }); 
                // THOUSAND SEPARATOR COMAS
            });
        </script>
    <!-- Input Number -->

    @yield('js')
</body>

</html>
