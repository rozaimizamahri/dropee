# REQUIREMENT
- Web Server
    - Apache 2.4 +
    - PHP 7.4 +
    - PostgreSQL 13 
    - MySQL
- Other
    - Visual Studio Code
    - Google Chrome Browser

# ENV SETUP
- Open your .env file
- set config as below

APP_NAME=Dropee
APP_ENV=local
APP_KEY=base64:Y7/K5B95bJ2zew80Y6rzxkg7b6AzC7gHm4Z/ARCAm/I=
APP_DEBUG=true
APP_URL=https://dropee-uat.com 

SESSION_SECURE_COOKIE=false
TIMEZONE='Asia/Kuala_Lumpur'

DB_CONNECTION      = pgsql
DB_HOST            = 127.0.0.1
DB_PORT            = 5432
DB_SCHEMA          = public
DB_DATABASE        = dropee    
DB_USERNAME        = 
DB_PASSWORD        =  

# TEST PROJECT
- open terminal 
- cd /projectname
- run 'php artisan migrate:fresh'
- run 'php artisan db:seed'
- run 'php artisan serve or open google chrome browser and type 'localhost/dropee'


# LARAVEL SAIL DOCKER
DOCKER
- download docker for MACOS
- open terminal 
- cd /Www/projectname 
- composer require laravel/sail —dev
- php artisan sail:install
- makesure stop httpd and mariadb in your macos running before running
- ./vendor/bin/sail up
- ./vendor/bin/sail down (to restart) the process on step 9 Wait until you see message running in port
- open browser and type ‘localhost’
- in .env change DB_HOST to 127.0.0.1
- php artisan migrate
- To access DB using Dbeaver, just type the same crendentials in .env
- makesure you stop httpd, postgresql-13, mariadb services in your laptop/mac